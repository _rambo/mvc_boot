test: jshint
	NODE_ENV=unittesting ./node_modules/.bin/istanbul cover node_modules/mocha/bin/_mocha

jshint:
	./node_modules/jshint/bin/jshint .

.PHONY: test
