├── bin                                      -----    存放执行文件(脚本)目录
│   └── etc
│       ├── init.d
│       └── nginx
├── config                                   -----    存放常量表 production.cson unittesting.production.cson development.production.cson testing.production.cson
├── controllers                              -----    controllers 逻辑
├── coverage                                 -----    单元测试报告
├── jobs                                     -----    定时任务
├── lib                                      -----    存放常用到的函数
│   ├── event_handlers                       -----    pub事件集合
│   └── jscs_rules                           -----    jscs 自定义规则
├── middleware                               -----    处理中间件
├── models                                   -----    Model文件夹
│   └── plugin
├── monitor                                  -----    程序监控
├── static
│   ├── apk                                  -----     android ios 客户端
│   ├── dist                                 -----     压缩文件 包括js img css
│   ├── images                               -----     images 文件夹
│   ├── javascripts                          -----     javascript 文件
│   │   └── i18n                             -----     i18n 文件
│   │   └── lib                              -----     存放前端用到的第三方库
	│   │   └── jquery                            ----     默认加载jquery
	│   │   └── require                           ----     默认加载require
│   └── sass                                 -----     使用sass作为css工具
│       └── common
├── test                                     -----     单元测试
└── views                                    -----     views
.jscsrc                                      -----     jscsrc 规则
.jshint                                      -----     jshint 规则
Makefile                                     -----     自动化
.gitignore                                   -----     git 忽略文件
